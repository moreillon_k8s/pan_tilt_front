import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)


export default new Vuex.Store({
  state: {
    devices: [],
    ws_connected: false,
  },
  mutations: {
    set_ws_connected(state, connected){
      state.ws_connected = connected
    },
    delete_and_create_devices(state, devices){

      state.devices.splice(0,state.devices.length)

      devices.forEach((device) => {
        state.devices.push(device)
      })

    },
    add_device(state, device){
      const find_function = (available_device) => {return available_device.id === device.id}

      if(!state.devices.find(find_function)){
        state.devices.push(device)
      }


    },
    delete_device(state,device){
      const find_function = (available_device) => {return available_device.id === device.id}

      let device_index = state.devices.findIndex(find_function)

      if(device_index !== -1){
        state.devices.splice(device_index,1)
      }

    }
  },
  actions: {
  },
  modules: {
  }
})
